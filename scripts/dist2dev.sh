#!/bin/bash

debug=""
convert_to=""

##################################################################################################################################
# DISPLAY HELP
#################################################################################################################################
function print_help() {
  printf "Use: dist2dev.sh [options] <mode> \n"
  printf "    [-h]                  Shows this help message and ends.\n"
  printf "    [-d]                  Enables debug messages.\n"
  printf "\nModes:\n"
  printf "       DIST: Converts the station to the DISTRIBUTION version.\n"
  printf "       DEV:  Converts the station to the DISTRIBUTION version.\n"
  exit 0
}

##################################################################################################################################
# PROMPT FOR REPODIR MANUALLY
#################################################################################################################################
function prompt_workdir() {
  local answered=""
  local done=""
  local continue
  local continue2

  printf "Repository folder not found.\n"
  printf "Do you want to input it manually?. [Y]es/[N]o: \n"
  # Chance to exit without enterin repodir
  while [ -z "$done" ]; do
    if ! read -r continue; then
      printf "Error reading STDIN. Aborting...\n"
      exit 1
    elif [[ "$continue" =~ [yY].* ]]; then
      # if yes prompt for repodir
      done="yes"
      while [ -z "$answered" ]; do
      printf "Folder in which the SEISREC-DIST is: \n"
      if ! read -r repodir; then
        printf "Error reading STDIN!. Aborting...\n"
        exit 1
      fi
      printf "Is \"%s\" correct? [Y]es, [N]o, [C]ancel: \n" "$repodir"
        # Confirm input
        if ! read -r continue2; then
          printf "Error reading STDIN!. Aborting...\n"
          exit 1
        elif [[ "$continue2" =~ [yY].* ]]; then
          answered="yes"
          return 0
        elif [[ "$continue2" =~ [nN].* ]]; then
          answered=""
        elif [[ "$continue2" =~ [cC].* ]]; then
          answered="no"
        else
          printf "\n[Y]es, [N]o , [C]ancel?: "
        fi
      done
    elif [[ "$continue" =~ [nN].* ]]; then
      done="no"
    else
      printf "\n¿[Y]es, [N]o?: "
    fi
  done
  return 1
}

##################################################################################################################################
# GET WORKING DIRECTORY
#################################################################################################################################
# automatic search for repo directory assuming dist2dev resides in /SEISREC-DIST/scripts/
if [ -z "$repodir" ]; then
  repodir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  repodir=$(printf "%s" "$repodir" | sed -e "s/\/SEISREC-DIST.*//")
  #repodir is the immediate parent directory to SEISREC-DIST
fi

# if repodir found, source script utils
if [ -n "$repodir" ]; then
  export repodir
  workdir="$repodir/SEISREC-DIST"
  source "$workdir/scripts/script_utils.sh"
else
  if ! prompt_workdir; then
    printf "Error getting the wroking folder. Aborting ...\n"
    exit 1
  fi
fi

if [ -n "$debug" ]; then
  printf "Folder = %s\n" "$workdir"
fi

# If im in the directory im going to delete then bail out
if [ -n "$(pwd | grep SEISREC-DEV)" ]; then
  printf "The fcurrent folder is inside the SEISREC-DEV folder!.\n"
  currdir="$workdir"
else
  currdir=$(pwd)
fi

# Parse options
while getopts "dh" opt; do
  case ${opt} in
  d)
    debug="yes"
    ;;
  h)
    print_help
    ;;
  \?)
    printf "Invalid option: -%s." "$OPTARG" 1>&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

# Parse arguments
while [ -n "$1" ]; do
  PARAM="${1,,}"
  if [ -n "$debug" ]; then
    printf "PARAM = %s\n" "$PARAM"
  fi
  if [ -z "$PARAM" ]; then
    print_help
  fi
  case $PARAM in
  "dist")
    convert_to="DIST"
    break
    ;;
  "dev")
    convert_to="DEV"
    break
    ;;
  "help")
    print_help
    break
    ;;
  \?)
    printf "Invalid argument: -%s". "$PARAM" 1>&2
    exit 1
    ;;
  esac
  shift
done
unset PARAM

if [ -n "$debug" ]; then
  printf "\$(pwd) = %s\n" "$(pwd)"
  printf "Current folder = %s\n" "$currdir"
fi

case $convert_to in
# If converting to DIST, delete DEV directory safely
"DIST")
  if [ -d "$workdir/SEISREC-DEV" ]; then
    printf "DEV folder does not exist ...\n"
    if ! cd "$workdir/SEISREC-DEV"; then
      printf "Error trying to access to ./SEISREC-DEV!.\n"
      exit 1
    fi
    # check for repository name to be sure were deleting the correct directory
    reponame=$(basename $(git rev-parse --show-toplevel))

    if [ -n "$debug" ]; then
      printf "Repository name = %s\n" "$reponame"
    fi

    # Notify if everything's in order, delete directory anyway
    if [ "$reponame" == "SEISREC-DEV" ]; then
      printf "SEISREC-DEV folder was detected. Removing ...\n"
    else
      printf "SEISREC-DEV folder was detected, but it contains the incorrect repository. Removing...\n"
    fi

    # Exit directory first
    if ! cd ..; then
        printf "Error trying to exiting from ./SEISREC-DEV!. Aborting ...\n"
        exit 1
      fi
      printf "Removing SEISREC-DEV ...\n"
      # remove with sudo, as git prevents user deleting repository files
      if ! sudo rm -r "SEISREC-DEV"; then
        printf "Error removing ./SEISREC-DEV!. Aborting ...\n"
        exit 1
    fi
  fi

  ;;
"DEV")
  # in converting to DEV, check if directory structure is broken, then clone.
  if [ -d "$workdir/SEISREC-DEV" ]; then
    printf "Folder DEV already exists ...\n"
    if ! cd "$workdir/SEISREC-DEV"; then
      printf "Error getting to access to ./SEISREC-DEV!.\n"
      exit 1
    fi
    # check for repository name to be sure were deleting the correct directory
    reponame=$(basename $(git rev-parse --show-toplevel))
    if [ -n "$debug" ]; then
      printf "Repository name = %s\n" "$reponame"
    fi

    # Check if SEISREC-DEV already present
    if [ "$reponame" == "SEISREC-DEV" ]; then
      printf "The station is already converted to DEV!. Exiting ...\n"
      exit 1 # Exit if there's any funny business with the filesystem
    else
      # If there's some error with the repository, delete directory and start fresh
      printf "SEISREC-DEV folder was detected, but it contains the incorrect repository. Removing ...\n"
      if ! cd ..; then
        printf "Error getting out of ./SEISREC-DEV!. Aborting ...\n"
        exit 1 # Exit if there's any funny business with the filesystem
      fi
      if ! sudo rm -r "SEISREC-DEV"; then
        printf "Error removing ./SEISREC-DEV!. Aborting ...\n"
        exit 1 # Exit if there's any funny business with the filesystem
      fi
    fi
  fi
  # move into SEISREC-DIST
  printf "Accessing to %s\n" "$workdir"
  if ! cd "$workdir"; then
    printf "Error trying to access to SEISREC-DIST!.\n"
    exit 1 # Exit if there's any funny business with the filesystem
  fi

  # Clone Directory
  printf "Cloning SEISREC-DEV ...\n"
  if ! git clone https://gitlab.com/alexbecerra/SEISREC-DEV.git; then
    printf "Error cloning ./SEISREC-DEV!.\n"
    exit 1 # Exit if there's any funny business with the filesystem
  fi

  ;;
\?)
  printf "Invalid argument: -%s" "$PARAM" 1>&2
  exit 1
  ;;
esac

if [ -n "$debug" ]; then
  printf "\$(pwd) = %s\n" "$(pwd)"
  printf "Current folder = %s\n" "$currdir"
fi

# move back out to original directory
if ! cd "$currdir"; then
  printf "Error getting back to %s!.\n" "$currdir"
  exit 1
fi

if [ -n "$debug" ]; then
  printf "\$(pwd) = %s\n" "$(pwd)"
  printf "Current folder = %s\n" "$currdir"
fi

unset PARAM

exit 0