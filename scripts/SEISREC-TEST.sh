#!/bin/bash

if [ -z "$repodir" ]; then
  repodir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  repodir=$(printf "%s" "$repodir" | sed -e "s/\/SEISREC-DIST.*//")
fi

if [ -n "$repodir" ]; then
  export repodir
  workdir="$repodir/SEISREC-DIST"
  source "$workdir/scripts/script_utils.sh"
else
  printf "Error getting the working directory!. Aborting...\n"
  exit 1
fi

dir="$repodir/SEISREC-DIST/TEST"

if [ -d "$dir" ]; then
TESTS=$(ls "$dir")

for t in $TESTS; do
  printf "Starting test for module %s ...\n" "$(printf "%s" "$t" | sed -e "s/TEST_//")"
  if ! sudo "$dir/$t"; then
    printf "Error in %s!.\n" "$t"
  fi
  printf "\n"
done
else
  printf "No tests available!.\n"
fi