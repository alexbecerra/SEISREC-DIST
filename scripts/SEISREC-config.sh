#!/bin/bash

debug=''
choice=""
done=""
sta_type="DIST"
other_sta_type="DEV"

# TODO: make space somewhere to add releases

# Parse options first and foremost
while getopts "dh" opt; do
  case ${opt} in
  d)
    debug="yes" # Set debug as early as possible
    ;;
  h)
    printf "Use: SEISREC-config.sh [options]"
    printf "    [-h]                  Shows this help message and exits.\n"
    printf "    [-d]                  Enable debug messages.\n"
    exit 0
    ;;
  \?)
    printf "Invalid option: -%s" "$OPTARG" 1>&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

##################################################################################################################################
# GET WORKING DIRECTORY - obtains directory where repo is stored
# ################################################################################################################################

# Get working directory from source directory of running script
if [ -z "$repodir" ]; then
  try_dist="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
  repodir=$(printf "%s" "$try_dist" | sed -e "s_/SEISREC-DIST.*__")
  if [ "$try_dist" == "$repodir" ]; then
    repodir=$(printf "%s" "$try_dist" | sed -e "s_/SEISREC-SERVER-DEV.*__")
    repo="SEISREC-SERVER-DEV"
  else
    repo="SEISREC-DIST"
  fi
  export repo
fi

# if the directory is found, export variable for scripts that are called later
if [ -n "$repodir" ]; then
  if [ -n "$debug" ]; then
    printf "repo = %s\n" "$repo"
    printf "repodir = %s\n" "$repodir"
  fi
  export repodir
  workdir="$repodir/SEISREC-DIST"
  # workdir variable declared for convenience
  if [ -n "$debug" ]; then
    printf "workdir = %s\n" "$workdir"
  fi

  # Sourcing script_utils.sh for utility bash functions
  if source "$workdir/scripts/script_utils.sh"; then
    printf "Establishing parameters for script_utils.sh...\n"
  else
    printf "Error establishing parameters for script_utils.sh!. Aborting...\n"
    exit 1
  fi
else
  printf "Error getting the working directory!. Aborting...\n"
  exit 1
fi

##################################################################################################################################
# CONFIGURE STATION PARAMS - function that calls util Param-edit for editing station parameters
# ################################################################################################################################
function configure_station() {
  local opts=()
  # specifying path to parameter file
  opts+=(-pth "$repodir/SEISREC-DIST/")
  # if debug print used options
  if [ -n "$debug" ]; then
    #if debug flag, call util param-edit with -debug
    opts+=(-debug)
    printf "opts = "
    for o in "${opts[@]}"; do
      printf "%s " "$o"
    done
    printf "\n"
  fi
  print_title "STATION PARAMETER CONFIGURATION - SEISREC-config.sh"
  "$repodir/SEISREC-DIST/util/util_paramedit" "${opts[@]}"
  any_key
}

##################################################################################################################################
# UPDATE SYSTEM SOFTWARE - updates to DIST and DEV software
# ################################################################################################################################
function update_station_software() {
  print_title "SYSTEM SOFTWARE UPDATE - SEISREC-config.sh"
  # sta_type variable must be defined
  if [ -z "$sta_type" ]; then
    printf "Station type not defined!.\n"
    exit 1
  fi

  local currdir=$(pwd)
  local answered=''
  local version=''
  local versionlist=''
  local selectedversion=''

  while [ -z "$answered" ]; do
    continue=""
    answered=""
    version=""
    print_title "SYSTEM UPDATE - SEISREC-config.sh"
    # Get last commit to SEISREC-DIST
    if [ -d "$workdir" ]; then
      if ! cd "$workdir"; then
        printf "Error accessing to %s.\n" "$workdir"
        exit 1
      else
        if git log | head -5 >/dev/null 2>&1; then
          printf "SEISREC-DIST - Last commit to branch %s:\n\n" "$(git branch | grep "\*.*" | sed -e "s/* //")"
          printf "%s\n\n" "$(git log | head -5)"
          version=""
          if git describe --tags >/dev/null 2>&1; then
            version=$(git describe --tags)
          fi
          if [ -n "$version" ]; then
            printf "\nSoftware version: %s.\n" "$version"
          fi
        else
          printf "Error getting git logs!.\n"
        fi
      fi
    else
      printf "SEISREC-DIST folder not found!.\n"
      exit 1
    fi
    printf "\n"

    # Get last commit to SEISREC-DEV
    if [ "$sta_type" == "DEV" ]; then
      if [ -d "$workdir/SEISREC-DEV" ]; then
        if ! cd "$workdir/SEISREC-DEV"; then
          printf "Error accessing to %s.\n" "$workdir/SEISREC-DEV"
          exit 1
        else
          if git log | head -5 >/dev/null 2>&1; then
            printf "\nSEISREC-DEV - Last commit to branch %s:\n\n" "$(git branch | grep "\*.*" | sed -e "s/* //")"
            printf "%s\n\n" "$(git log | head -5)"
            version=""
            if git describe --tags >/dev/null 2>&1; then
              version=$(git describe --tags)
            fi
            if [ -n "$version" ]; then
              printf "\nSoftware version: %s.\n" "$version"
            fi
          else
            printf "Error getting git logs!.\n"
          fi
        fi
      else
        printf "SEISREC-DEV folder not found!.\n"
        exit 1
      fi
    fi
    printf "\n"

    # Select update by version or simple git pull
    PS3='Select: '
    if [ "$sta_type" == "DEV" ]; then
      options=("Software version DIST" "Software version DEV" "Manual update" "Back")
    else
      options=("Software version DIST" "Manual update" "Back")
    fi
    select opt in "${options[@]}"; do
      case $opt in
      "Manual update")
      # Manual update pulls most recent commit from remote
      print_title "Manual update"
        while [ -z "$continue" ]; do
          if ! read -r -p "Update station? [Y]es/[N]o: " continue; then
            printf "Error reading STDIN!. Aborting...\n"
            exit 1
          elif [[ "$continue" =~ [yY].* ]]; then
            if [ -d "$workdir" ]; then
              if ! cd "$workdir"; then
                printf "Error accessing to %s.\n" "$workdir"
                exit 1
              fi

              printf "Getting changes from remote folder SEISREC-DIST...\n\n"
              if [ $(git rev-parse --abbrev-ref --symbolic-full-name HEAD) == "HEAD" ]; then
                  git checkout master
              fi
              git pull

              if [ "$sta_type" == "DEV" ]; then
                if [ -d "$workdir/SEISREC-DEV" ]; then
                  if ! cd "$workdir/SEISREC-DEV"; then
                    printf "Error accessing to %s/SEISREC-DEV.\n" "$workdir"
                    exit 1
                  fi

                  printf "\nGetting changes from remote folder SEISREC-DEV...\n\n"
                  git pull
                else
                  printf "%s/SEISREC-DEV not found!.\n" "$workdir"
                fi
              fi
            else
              printf "%s/SEISREC-DEV not found!.\n" "$workdir"
            fi
            break
          elif [[ "$continue" =~ [nN].* ]]; then
            break
          else
            continue=""
          fi
        done
        any_key
        break
        ;;

      "Software version DIST")
        # Print current DIST version
        while [ -z "$continue" ]; do
          print_title "Update software version DIST"
          if ! cd "$workdir"; then
            printf "Error accessing to %s!.\n" "$workdir"
          fi
          if git describe --tags >/dev/null 2>&1; then
            version=$(git describe --tags)
            printf "Current DIST software version: %s.\n" "$version"
          else
            printf "Current commit does not have tags.\n"
          fi

          printf "\n"
          if ! read -r -p "Change DIST software version? [Y]es/[N]o: " continue; then
            printf "Error reading STDIN!. Aborting ...\n"
            exit 1
          elif [[ "$continue" =~ [sS].* ]]; then
            # Get and print list of tags to checkout
            versionlist=$(git tag -l)
            if [ -z "$versionlist" ]; then
              printf "Versions not found!.\n"
              any_key
              break
            fi

            PS3='Select version: '
            options=()
            for f in $(printf "%s" "$versionlist" | sed -e 's/$version\n//'); do
              options+=( "$f" )
            done
            options+=( "Exit" )
            select opt in "${options[@]}"; do
              if [ -n "$debug" ]; then
                printf "opt = %s\n" "$opt"
              fi

              # Try checking out tag
              if [ "$opt" == "Exit" ]; then
                  break
              elif ! git checkout "tags/$opt"; then
                printf "Error updating the software!.\n"
                continue=''
                any_key
                break
              fi
              any_key
              break
            done
          elif [[ "$continue" =~ [nN].* ]]; then
            break
          else
            continue=""
          fi
        done
        break
        ;;

      "Software version DEV")
        # Print current DEV version
        while [ -z "$continue" ]; do
          print_title "Update software version DEV"
          if [ "$sta_type" != "DEV" ]; then
            printf "Error in station type!.\n"
            exit 1
          fi

          if ! cd "$workdir/SEISREC-DEV"; then
            printf "Error accessing to %s!.\n" "$workdir"
          fi
          if git describe --tags >/dev/null 2>&1; then
            version=$(git describe --tags)
          printf "Current DEV software version: %s.\n" "$version"
          else
            printf "Current commit does not have tags.\n"
          fi
          printf "\n"

          if ! read -r -p "Change DEV software version? [Y]es/[N]o: " continue; then
            printf "Error reading STDIN!. Aborting...\n"
            exit 1
          elif [[ "$continue" =~ [sS].* ]]; then
            # Get and print list of tags to checkout
            # But first, delete all local tags and get the new list
            # TODO: Check this code
            # https://stackoverflow.com/questions/1841341/remove-local-git-tags-that-are-no-longer-on-the-remote-repository
            # git tag -l | xargs git tag -d
            # git fetch --tags
            versionlist=$(git tag -l)
            if [ -z "$versionlist" ]; then
              printf "¡No se encontraron versiones!.\n"
              any_key
              break
            fi

            PS3='Select DEV software version: '
            options=()
            for f in $(printf "%s" "$versionlist" | sed -e 's/$version\n//'); do
              options+=( "$f" )
            done
            options+=( "Exit" )
            select opt in "${options[@]}"; do

              if [ -n "$debug" ]; then
                printf "opt = %s" "$opt"
              fi

              # Try checking out tag
              if [ "$opt" == "Exit" ]; then
                  break
              elif ! "git checkout tags/$opt"; then
                printf "Error updating the software!.\n"
                continue=''
                any_key
                break
              fi
            done
            continue=""
            break
          elif [[ "$continue" =~ [nN].* ]]; then
            break
          else
            continue=""
          fi
          break
        done
        break
        ;;

      "Back")
        answered="yes"
        break
        ;;
      esac
    done
  done

  if [ -d "$currdir" ]; then
    if ! cd "$currdir"; then
      printf "Error accessing to %s.\n" "$currdir"
      exit 1
    fi
  else
    printf "Folder %s not found!.\n" "$currdir"
  fi
}
##################################################################################################################################
# MANAGE SERVICES
# ################################################################################################################################
function manage_services() {
  local PS3
  local options
  local opt
  local choice
  local REPLY
  local menu_title
  local answered

  while [ -z "$answered" ]; do
    choice=""
    print_title "SERVICE CONFIGURATION - SEISREC_config.sh"

    # Get enabled services
    enabled_services=$(systemctl list-unit-files)

    # Get SEISREC services
    services=$(ls "$repodir/SEISREC-DIST/services")
    printf "\nServices status:\n"
    for s in $services; do
      if [ -n "$debug" ]; then
        printf "s = %s\n" "$s"
      fi
      servcheck=$(printf "%s" "$enabled_services" | grep "$s")
      if [ -n "$debug" ]; then
        printf "servcheck = %s\n" "$servcheck"
      fi
      if [ -n "$servcheck" ]; then
        printf "%s\n" "$servcheck"
      fi
    done

    # Assemble selected services file if it doesn't exist
    if [ ! -f "$workdir/selected_services_file.tmp" ]; then
      printf "%s" "$(ls "$repodir/SEISREC-DIST/services" | grep ".*.service")" >>"$workdir/selected_services_file.tmp"
    fi

    # Get list from temp file for display
    local list
    if [ -f "$workdir/selected_services_file.tmp" ]; then
      list=$(cat "$workdir/selected_services_file.tmp")
      printf "\nSelect services for configuration: "
      for l in $list; do
        printf "%s " "$l"
      done
      printf "\n"
    fi

    local opts=()
    if [ -n "$debug" ]; then
      opts+=("-d")
    fi
    opts+=(-f "$workdir/selected_services_file.tmp")

    local name=""
    # Select action for services and run install_services.sh
    PS3='Select: '
    options=("Start" "Stop" "Disable" "Clean" "Install" "Select services" "Enable debug mode" "Disable debug mode" "Back")
    select opt in "${options[@]}"; do
      case $opt in
      "Start")
        choice="Start"
        opts+=("${choice,,}")
        if [ -n "$debug" ]; then
          printf "opts = "
          printf "%s " "${opts[@]}"
          printf "\n"
        fi
        if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
          printf "Error modifying services!\n"
        fi
        any_key
        break
        ;;
      "Stop")
        choice="Stop"
        opts+=("$choice")
        if [ -n "$debug" ]; then
          printf "opts = "
          printf "%s " "${opts[@]}"
          printf "\n"
        fi
        if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
          printf "Error modifying services!\n"
        fi
        any_key
        break
        ;;
      "Disable")
        choice="Disable"
        opts+=("$choice")
        if [ -n "$debug" ]; then
          printf "opts = "
          printf "%s " "${opts[@]}"
          printf "\n"
        fi
        if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
          printf "Error modifying services!\n"
        fi
        any_key
        break
        ;;
      "Clean")
        choice="Clean"
        opts+=("$choice")
        if [ -n "$debug" ]; then
          printf "opts = "
          printf "%s " "${opts[@]}"
          printf "\n"
        fi
        if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
          printf "Error modifying services!\n"
        fi
        any_key
        break
        ;;
      "Install")
        choice="Install"
        opts+=("$choice")
        if [ -n "$debug" ]; then
          printf "opts = "
          printf "%s " "${opts[@]}"
          printf "\n"
        fi
        if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
          printf "Error modifying services!\n"
        fi
        any_key
        break
        ;;
      "Select services")
        printf "%s" "$(ls $repodir/SEISREC-DIST/services | grep ".*.service")" >>"$workdir/available_services.tmp"
        select_several_menu "SELECT SERVICES - SEISREC-config.sh" "$workdir/available_services.tmp" "$workdir/selected_services_file.tmp"
        break
        ;;
      "Enable debug mode")
        for s in $list; do
          name=$(printf "%s" "$s" | sed "s/.service//g")
          if ! sed -i "s/$name/$name -d/" "$repodir/SEISREC-DIST/services/$s"; then
              printf "Error enabling debug on %s! Aborting...\n" "$s"
          fi
        done
        if ! sudo systemctl daemon-reload; then
          printf "Error loading modified services!\n"
        fi
        break
        ;;
      "Disable debug mode")
        for s in $list; do
          name=$(printf "%s" "$s" | sed "s/.service//g")
          if ! sed -i "s/$name -d/$name/" "$repodir/SEISREC-DIST/services/$s"; then
              printf "Error disabling debug on %s! Aborting...\n" "$s"
          fi
        done
        if ! sudo systemctl daemon-reload; then
          printf "Error loading modified services!\n"
        fi
        break
        ;;
      "Back")
        answered="yes"
        printf "Cleaning and exiting...\n"
        clean_up "$workdir/available_services.tmp"
        clean_up "$workdir/selected_services_file.tmp"
        if [ -n "$debug" ]; then
          printf "¡Hasta luego!.\n"
        fi
        break
        ;;
      *)
        printf "Invalid option %s.\n" "$REPLY"
        break
        ;;
      esac
    done
  done
}

##################################################################################################################################
# GET SOFTWARE INFO FUNCTION
# ################################################################################################################################
function get_software_info() {
  print_title "SOFTWARE DETAILED INFORMATION - SEISREC-config.sh"

  if [ -z "$sta_type" ]; then
    printf "Station type not defined!.\n"
    exit 1
  fi

  # Get current working directory for return point
  local currdir=$(pwd)

  if [ -d "$workdir" ]; then
    if ! cd "$workdir"; then
      printf "Error accessing to %s.\n" "$workdir"
      exit 1
    else
      # Get last commit info
      if git log | head -5 >/dev/null 2>&1; then
        printf "SEISREC-DIST - Last commit to branch %s:\n\n" "$(git branch | grep "\*.*" | sed -e "s/* //")"
        printf "%s" "$(git log | head -5)"
      else
        printf "¡rror getting git logs!.\n"
      fi
    fi
  else
    printf "SEISREC-DIST folder not found!\n"
    exit 1
  fi
  printf "\n"
  if [ "$sta_type" == "DEV" ]; then
    if [ -d "$workdir/SEISREC-DEV" ]; then
      if ! cd "$workdir/SEISREC-DEV"; then
        printf "Error accessing to %s.\n" "$workdir/SEISREC-DEV"
        exit 1
      else
        # Get last commit info
        if git log | head -5 >/dev/null 2>&1; then
          printf "SEISREC-DEV - Last commit to branch %s:\n\n" "$(git branch | grep "\*.*" | sed -e "s/* //")"
          printf "%s\n\n" "$(git log | head -5)"
        else
          printf "Error getting git logs!.\n"
        fi
      fi
    else
      printf "SEISREC-DEV folder not found!.\n"
      exit 1
    fi
  fi

  # Display Info

  print_exec_versions

  printf "Installed system software versions:\n\n"

  printf "Python version: %s" "$(python3 --version | sed -e "s/Python //")"

  printf "\n"
  printf "Redis Server version: %s\n" "$(redis-server -v | grep -o "v=.* sha" | sed -e "s/v=//" | sed -e "s/sha//")"
  printf "Redis Client version: %s\n" "$(redis-cli -v | grep -o " .*$" | sed -e "s/ //")"

  printf "\n"
  printf "MRAA C lib version: %s\n" "$(sudo ldconfig -v 2>&1 | grep mraa | tail -1 | grep -m2 -o "> libmraa.so.*.$" | sed -e "s/> libmraa.so.//")"
  printf "hiredis C lib version: %s\n" "$(sudo ldconfig -v 2>&1 | grep hiredis | tail -1 | grep -m2 -o "> libhiredis.so.*.$" | sed -e "s/> libhiredis.so.//")"
  ## TODO: ADD PIGPIO

  printf "\n"

  if [ "$sta_type" == "DEV" ]; then
    printf "\n"
    printf "Hiredis Python %s\n" "$(pip3 show hiredis | grep Version)"
    printf "Pyinstaller %s\n" "$(pip3 show pyinstaller | grep Version)"
  fi

  printf "\n"
  printf "NTP version: %s\n" "$(dpkg -l | grep "hi  ntp" | grep -o "1:....." | sed -e "s/1://")"
  printf "GPSD version: %s\n" "$(gpsd -V | grep -o "revision.*)" | sed -e "s/revision //" | sed -e "s/)//")"

  # Return to working directory
  if [ -d "$currdir" ]; then
    if ! cd "$currdir"; then
      printf "Error accessing to %s.\n" "$currdir"
      exit 1
    fi
  else
    printf "%s folder not found!.\n" "$currdir"
  fi

  any_key
}

##################################################################################################################################
# STATION SETUP FUNCTION
# ################################################################################################################################
function setup_station() {
  local cfgeverywhere=""

  print_title "STATION CONFIGURATION - SEISREC-config.sh"

  printf "Preparing configuration...\n"
  printf "Checking updates...\n"
  # Update Station software
  update_station_software

  printf "Configuring station parameters...\n"
  # Set up station parameters for operation
  configure_station

  # Install Services after configuring parameters
  printf "Installing services...\n"
  local opts=("INSTALL")
  if [ -n "$debug" ]; then
    opts+=(-d)
  fi
  if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
    printf "Error installing services!. Please, correct problems before trying again.\n"
    exit 1
  fi

  # Prompt for installing SEISREC-config utility
  if ! read -r -p "Install SEISREC-config in system PATH? [Y]es/[N]o: " continue; then
    printf "Error reading STDIN!. Aborting...\n"
    exit 1
  elif [[ "$continue" =~ [yY].* ]]; then
    cfgeverywhere="yes"
  elif [[ "$continue" =~ [nN].* ]]; then
    cfgeverywhere=""
  fi

  if [ -n "$cfgeverywhere" ]; then
    # if symlink to SEISREC-config doesn't exist, create it
    if [ ! -h "$repodir/SEISREC-DIST/SEISREC-config" ]; then
      printf "Creating symbolic link to SEISREC-config ...\n"
      ln -s "$repodir/SEISREC-DIST/scripts/SEISREC-config.sh" "$repodir/SEISREC-DIST/SEISREC-config"
    fi

    if ! cp "$HOME/.bashrc" "$HOME/.bashrc.bak"; then
      printf "Error creating .bashrc backup!.\n"
    fi

    # Check if ~/SEISREC is in PATH, if not, add it to PATH
    inBashrc=$(cat "$HOME/.bashrc" | grep 'SEISREC-DIST')
    inPath=$(printf "%s" "$PATH" | grep 'SEISREC-DIST')
    if [ -z "$inBashrc" ]; then
      if [ -z "$inPath" ]; then
        # Add it permanently to path
        printf "Adding ./SEISREC-DIST to system PATH...\n"
        printf "inPath=\"\$(printf \"\$PATH\" | grep \"%s/SEISREC-DIST\")\"\n" "$repodir" >>~/.bashrc
        printf 'if [ -z "$inPath" ]\n' >>~/.bashrc
        printf 'then\n' >>~/.bashrc
        printf '  export PATH="%s/SEISREC-DIST:$PATH"\n' "$repodir" >>~/.bashrc
        printf 'fi\n' >>~/.bashrc
        ## MODIFIED FOR ANY SERVICES INSTALLED
        printf "\n\nalias servcheck='sudo systemctl status neom8.service ; sudo systemctl status adxl355.service ; sudo systemctl status dyndns-manager.service ; sudo systemctl status db2file.service ; sudo systemctl status ds3231sn.service ; sudo systemctl status redis_6379_2.service ; sudo systemctl status ntp2.service'" >>~/.bashrc
      fi
    fi


  fi
  printf "The station will be rebooted... now!\n"
  any_key
  sudo reboot
  any_key
}

##################################################################################################################################
# DIST 2 DEV
# ################################################################################################################################
function dist2dev() {
  print_title "CONVERSION from $sta_type to $other_sta_type - SEISREC_config"
  local opts=()
  if [ -n "$debug" ]; then
    opts+=(-d)
  fi
  opts+=("$other_sta_type")

  # Automatically switch software version
  "$repodir/SEISREC-DIST/scripts/dist2dev.sh" "${opts[@]}"
  any_key
}

##################################################################################################################################
# SEISREC_build
# ################################################################################################################################
function SEISREC-build() {
  local opts=()
  if [ -n "$debug" ]; then
    opts+=(-d)
  fi
  # Build software using SEISREC-BUILD
  "$repodir/SEISREC-DIST/SEISREC-DEV/scripts/SEISREC_build.sh" "${opts[@]}"
}

##################################################################################################################################
# MANAGE NTP
# ################################################################################################################################
function manage_ntp() {
  local PS3
  local options
  local opt
  local choice
  local REPLY
  local answered

  while [ -z "$answered" ]; do
    choice=""
    print_title "NTP SERVICE CONFIGURATION - SEISREC_config"

    local opts=()
    if [ -n "$debug" ]; then
      opts+=(-d)
    fi
    PS3='Selection: '
    options=("Edit NTP conf file" "Load NTP conf file from DIST" "See NTP conf file" "Back")
    select opt in "${options[@]}"; do
      case $opt in
      "Edit NTP conf file")
        if [ -f "/etc/ntp.conf" ]; then
          if ! sudo nano /etc/ntp.conf; then
            printf "Error editing /etc/ntp.conf!.\n"
          fi
        else
          printf "/etc/ntp.conf not found!.\n"
        fi
        break
        ;;
      "Load NTP conf file from DIST")
        local ans
        while [ -z "$continue" ]; do
          if ! read -r -p "This action will OVERWRITE existing ntp.conf file. Do you want to continue? [Y]es/[N]o: " continue; then
            printf "Error reading STDIN!. Aborting...\n"
            exit 1
          elif [[ "$continue" =~ [sS].* ]]; then
            if [ -f "/etc/ntp.conf" ]; then
              if ! sudo rm "/etc/ntp.conf"; then
                printf "Error removing /etc/ntp.conf!.\n"
              fi
            fi
            if ! cp "$workdir/sysfiles/ntp.conf" "/etc/ntp.conf"; then
              printf "Error copying /etc/ntp.conf!.\n"
              any_key
            fi
            break
          elif [[ "$continue" =~ [nN].* ]]; then
            break
          else
            continue=""
          fi
        done
        break
        ;;
      "See NTP conf file")
        if [ -f "/etc/ntp.conf" ]; then
          if ! sudo cat "/etc/ntp.conf"; then
            printf "Error reading /etc/ntp.conf!.\n"
          fi
        else
          printf "/etc/ntp.conf not found!.\n"
        fi
        any_key
        break
        ;;
      "Back")
        answered="yes"
        break
        ;;
      *)
        printf "Invalid option %s.\n" "$REPLY"
        break
        ;;
      esac
    done
  done
  #under_construction
}

##################################################################################################################################
# MANAGE NETWORKS
# ################################################################################################################################
function manage_networks() {
  local PS3
  local options
  local opt
  local choice
  local REPLY
  local answered

  while [ -z "$answered" ]; do
    choice=""
    print_title "NETWORK CONFIGURATION - SEISREC_config"

    local opts=()
    if [ -n "$debug" ]; then
      opts+=(-d)
    fi
    PS3='Selection: '
    options=("Interface IP configuration" "Interface propierties configuration" "Back")
    select opt in "${options[@]}"; do
      case $opt in
      "Interface IP configuration")
        under_construction
        # TODO: Write IP config function
        any_key
        break
        ;;
      "Interface propierties configuration")
        if [ -f "$workdir/sysfiles/dhcpcd.conf" ]; then
          if ! sudo nano "$workdir/sysfiles/dhcpcd.conf"; then
            printf "Error editing %s/sysfiles/dhcpcd.conf!.\n" "$workdir"
            any_key
          fi
        else
          printf "%s/sysfiles/dhcpcd.conf not found!.\n" "$workdir"
          any_key
        fi
        break
        ;;
      "Back")
        answered="yes"
        break
        ;;
      *)
        printf "Invalid option %s.\n" "$REPLY"
        break
        ;;
      esac
    done
  done
}

##################################################################################################################################
# PERFORMANCE REPORT
# ################################################################################################################################
function performance_report() {
  print_title "PERFORMANCE REPORT - SEISREC_config"
  # TODO: write performance tests
  under_construction
  any_key
}
##################################################################################################################################
# UNINSTALL SEISREC
# ################################################################################################################################
function uninstall_seisrec() {
  print_title "UNINSTALL SOFTWARE - SEISREC_config"
  local currdir=$(pwd)

  if [ -n "$(pwd | grep "SEISREC-DIST")" ]; then
    if [ -n "$debug" ]; then
      printf "Current working folder is inside SESIREC-DIST"
    fi
    cd "$HOME"
  fi

  local opts=()
  if [ -n "$debug" ]; then
    opts+=(-d)
  fi
  while [ -z "$continue" ]; do
    if ! read -r -p "This action will UNINSTALL COMPLETELY ALL SEISREC software. Do you want to continue? [Y]es/[N]o: " continue; then
      printf "Error reading STDIN!. Aborting...\n"
      exit 1
    elif [[ "$continue" =~ [yY].* ]]; then
      choice="disable"
      opts+=(-n "$choice")
      if ! "$repodir/SEISREC-DIST/scripts/install_services.sh" "${opts[@]}"; then
        printf "Error disabling services!.\n"
      fi

      if ! rm "$HOME/.bashrc"; then
        printf "Error removing .bashrc! file.\n"
      fi

      if ! mv "$HOME/.bashrc.bak" "$HOME/.bashrc"; then
        printf "Error restoring original .bashrc file!.\n"
      fi

      export PATH=$(printf "%s" "$PATH" | sed -e "s|$repodir/SEISREC-DIST:||")

      if ! sudo rm -r "$repodir/SEISREC-DIST/"; then
        printf "Error removing SEISREC-DIST repository!.\n"
        exit 1
      fi

      printf "To reinstall the software, clone from the following repository https://gitlab.com/alexbecerra/SEISREC-DIST.git\n"
      any_key
      exit 0

    elif [[ "$continue" =~ [nN].* ]]; then
      break
    else
      continue=""
    fi
  done
  any_key
}

##################################################################################################################################
# UTILITIES
# ################################################################################################################################
function check_sta_type() {

  if [ -d "$repodir/SEISREC-DIST/SEISREC-DEV/" ]; then
    currdir=$(pwd)
    if ! cd "$repodir/SEISREC-DIST/SEISREC-DEV/"; then
      printf "Error accessing SEISREC-DEV folder!.\n"
    fi
    reponame=$(basename $(git rev-parse --show-toplevel))
    if [ "$reponame" == "SEISREC-DEV" ]; then
      sta_type="DEV"
      other_sta_type="DIST"
    else
      printf "SEISREC-DEV folder is present but it has an incorrect repository.\n"
    fi
  else
    sta_type="DIST"
    other_sta_type="DEV"
  fi
}
#*********************************************************************************************************************************
# MAIN BODY
#*********************************************************************************************************************************

print_title
print_banner
printf "\n"
any_key

if [ -n "$debug" ]; then
  printf "repodir = %s\n" "$repodir"
fi

check_sta_type
#=================================================================================================================================
# CLEAN UP FUNCTION
#=================================================================================================================================

while [ -z "$done" ]; do

  print_title "MAIN MENU - SEISREC_config"

  message=$(/etc/update-motd.d/01-tomo)
  echo "=== SYSTEM IMAGE INFORMATION ==="
  echo "$message"
  echo ""
  echo ""

  disco=$(df -h)
  echo "=== INFORMACION DEL DISCO DEL SISTEMA ==="
  echo "$disco"
  echo ""
  echo ""

  choice=""
  PS3='Selection: '
  options=("Configuration and software update" "Information and station tests" "Advanced options" "Exit")
  select opt in "${options[@]}"; do
    case $opt in
    "Advanced options")
      choice="Advanced options"
      break
      ;;
    "Information and station tests")
      choice="Information and station tests"
      break
      ;;
    "Configuration and software update")
      choice="Configuration and software update"
      break
      ;;
    "Exit")
      printf "Good bye!.\n"
      exit 0
      ;;
    *)
      printf "Invalid option %s.\n" "$REPLY"
      break
      ;;
    esac
  done

  if [ -n "$debug" ]; then
    printf "choice = %s\n" "$choice"
  fi

  #=================================================================================================================================
  # CLEAN UP FUNCTION
  #=================================================================================================================================
  case $choice in
  #-------------------------------------------------------------------------------------------------------------------------------
  # Advanced Options
  #-------------------------------------------------------------------------------------------------------------------------------}
  # TODO: Add gps network state
  "Advanced options")
    done=""
    if [ ! -f "$repodir/SEISREC-DIST/parameter" ]; then
      printf "Valid parameter file not found!. Please, execute station configuration first.\n"
      any_key
    else
      while [ -z "$done" ]; do
        check_sta_type

        print_title "STATION SOFTWARE CONFIGURATION - SEISREC_config.sh"
        if [ "$sta_type" == "DEV" ]; then
          options=("Station parameter configuration" "Services configuration" "Network configuration" "NTP configuration" "GPS status" "Convert to $other_sta_type" "Compile station software" "Back")
        else
          options=("Station parameter configuration" "Services configuration" "Network configuration" "NTP configuration" "GPS status" "Convert to $other_sta_type" "Back")
        fi
        select opt in "${options[@]}"; do
          case $opt in
          "Station parameter configuration")
            configure_station
            break
            ;;
          "Services configuration")
            manage_services
            break
            ;;
          "Network configuration")
            manage_networks
            break
            ;;
          "Convert to $other_sta_type")
            dist2dev
            break
            ;;
          "Compile station software")
            SEISREC-build
            break
            ;;
          "NTP configuration")
            manage_ntp
            break
            ;;
          "GPS status")
            watch -n 1 ntpq -c peer -c as -c rl
            break
            ;;
          "Back")
            done="yes"
            break
            ;;
          *)
            printf "Invalid option %s.\n" "$REPLY"
            break
            ;;
          esac
        done
      done
      done=""
    fi
    ;;
    #-------------------------------------------------------------------------------------------------------------------------------
    # Station Info & Tests
    #-------------------------------------------------------------------------------------------------------------------------------
  "Information and station tests")
    done=""
    while [ -z "$done" ]; do
      print_title "STATION INFORMATION AND TESTS - SEISREC_config.sh"
      options=("Execute station tests" "Software detailed information" "Performance report" "Back")
      select opt in "${options[@]}"; do
        case $opt in
        "Execute station tests")
          "$repodir/SEISREC-DIST/scripts/SEISREC-TEST.sh"
          any_key
          break
          ;;
        "Software detailed information")
          get_software_info
          break
          ;;
        "Performance report")
          performance_report
          break
          ;;
        "Back")
          done="yes"
          break
          ;;
        *)
          printf "Invalid option %s.\n" "$REPLY"
          break
          ;;
        esac
      done
    done
    done=""
    ;;
    #-------------------------------------------------------------------------------------------------------------------------------
    # Software Setup & Update
    #-------------------------------------------------------------------------------------------------------------------------------
  "Configuration and software update")
    done=""
    while [ -z "$done" ]; do
      continue=""
      print_title "STATION CONFIGURATION - SEISREC_config.sh"
      if [ ! -f "$repodir/SEISREC-DIST/parameter" ]; then
        printf "Station is NOT configured.\n"
        while [ -z "$continue" ]; do
          if ! read -r -p "Proceed with station configuration [Y]es/[O]mit: " continue; then
            printf "Error reading STDIN!. Aborting...\n"
            exit 1
          elif [[ "$continue" =~ [yY].* ]]; then
            setup_station
          elif [[ "$continue" =~ [oO].* ]]; then
            break
          else
            continue=""
          fi
        done
      fi
      print_title "SOFTWARE CONFIGURATION AND UPDATE - SEISREC_config.sh"

      options=("SEISREC version and update" "Station configuration" "Uninstall" "Back")
      select opt in "${options[@]}"; do
        case $opt in
        "SEISREC version and update")
          update_station_software
          break
          ;;
        "Configuración de la estación")
          if [ -f "$repodir/SEISREC-DIST/parameter" ]; then
            printf "It seems that the station is not configured.\n"
            if ! read -r -p "Do you want to reconfigure the station from the start? [Y]es/[N]o: " continue; then
              printf "Error reading STDIN! Aborting...\n"
              exit 1
            elif [[ "$continue" =~ [yY].* ]]; then
              if ! read -r -p "This action will OVERWRITE current station configuration. Do you want to continue? [Y]es/[N]o: " continue; then
                printf "Error reading STDIN! Aborting...\n"
                exit 1
              elif [[ "$continue" =~ [yY].* ]]; then
                clean_up "$repodir/SEISREC-DIST/parameter"
              elif [[ "$continue" =~ [nN].* ]]; then
                break
              fi
            elif [[ "$continue" =~ [nN].* ]]; then
              break
            fi
          fi
          setup_station
          break
          ;;
        "Uninstall")
          uninstall_seisrec
          break
          ;;
        "Back")
          done="yes"
          break
          ;;
        *)
          printf "Invalid option %s.\n" "$REPLY"
          break
          ;;
        esac
      done
    done
    done=""
    ;;
  esac
done
printf "Good bye!.\n"
exit 0
